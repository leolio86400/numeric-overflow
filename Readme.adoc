= Numeric Overflow

//tag::abstract[]

Number overflow happens
when an arithmetic operation results into
a numeric value that is outside of the
range for the represented value.
Due to modulus arithmetic, the value
wraps around. This can result
in not anticipated numeric value in the context
of the program.

JavaScript Typed Arrays use variety of ranges,
from 8bits (`Int8Array`) to 64bit (`BigInt64Array`).

For example,
`Int32Array` is signed 32 bits, therefore it ranges
between -2,147,483,648 to 2,147,483,647.

If a given value is bigger or smaller than Integer
range, the variable overflows and turn to a negative
or positive numbers respectively.

This issue can cause logic bypass and often
result in serious impact.

This vulnerabilities affects all numeric data types
that do no perform range validation.

Please note JavaScript Number type (default)
is not effected by this vulnerability. It
is however, impact by a similar security bug.

//end::abstract[]

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

=== Task 0

*Fork* and clone this repository.
Install `docker` and `make` on your system.

. Build the program: `make build`.
. Run it: `make run`.
. Run unit tests: `make test`. 
. Run security tests: `make securitytest`.

Note: The last test will fail. 

=== Task 1

Review the program code try to find out 
why security tests fails. 

Note: Avoid looking at tests or patch file and try to
spot the vulnerable pattern on your own.

=== Task 2

The program is vulnerable to Numeric Overflow. 
Find how to patch this vulnerability.

=== Task 3

Review `test/appSecurity.test.js` and see how security tests
works. Review your patch from Task 2.
Make sure this time the security tests pass.
If you stuck, move to the next task.

=== Task 4

Check out the `patch` branch:

* Read link:cheatsheet.adoc[] and apply to your solution.
* Review the patched program.
* Run all tests.

=== Task 5

Push you code and make sure build is passed.
Finally, send a pull request (PR).

//end::lab[]

//tag::references[]

== References

* OWASP Application Security Verification Standard 4.0, 5.4.3
* CWE 190
* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures[JavaScript data types and data structures]

//Contributing
include::CONTRIBUTING.adoc[]

== License

See link:LICENSE[]
