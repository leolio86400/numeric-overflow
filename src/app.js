'use strict';

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Main 
app.get('/', (req, res) => {
    try {
        var amount = getQueryParameterNumber(req, 'amount');
        if (approval(amount)) {
            return res.status(400).end(req.query.amount + ' requires approval.');
        } else {
            return res.status(200).end(req.query.amount + ' does not require approval.');
        }
    } catch (err) {
        console.log(err);
        return res.status(400).end('nope');
    }
});

var getQueryParameterNumber = (req, key) => {
    // empty
    if (!req || !req.query || !req.query.hasOwnProperty(key)) {
        throw Error('missing parameter');
    }
    var value = req.query[key];
    if (typeof value === 'number') {
        if (value > Number.MAX_SAFE_INTEGER) {
            throw Error('Incorrect parameter');
        }
        return value;
    }
    if (typeof value !== 'string') {
        throw Error('wrong type parameter');
    }
    // string larger than MAX_SAFE_INTEGER
    if (value.length > 16) {
        throw Error('String parameter too large');
    }
    value = parseInt(value);
    if (isNaN(value) || value > Number.MAX_SAFE_INTEGER) {
        throw Error('Incorrect parameter');
    }
    return value;
};

var validateNumber = (value, min, max) => {
    if (typeof value !== 'number' || isNaN(value)) {
        throw new Error('wrong parameter');
    }
    if (value < min || value > max) {
        throw RangeError();
    }
    return true;
};

// Transaction approval
// If an amount is less than a threashold
// approval is not required
var approval = (value) => {
    var surcharge = 10;
    var threshold = 1000;
    validateNumber(value, 0, 2147483647 - surcharge);
    return value >= (threshold - surcharge);
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, approval };
